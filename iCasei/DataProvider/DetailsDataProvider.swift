//
//  DetailsDataProvider.swift
//  iCasei
//
//  Created by Taize Carminatti on 11/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import Foundation

class DetailsDataProvider {
    
    var delegate: DetailsDataProviderDelegate?
    
    func getDetails(bySearchString string: String? = nil) {
        let apiManager = ApiManager()
        var parameters = ["part" : "snippet,statistics"]
        
        if string != nil {
            parameters["id"] = string!
        }
        apiManager.get(Endpoint.getDetails, parameters: parameters) { (success, response) in
            if !success {
                self.delegate?.detailsDataProvider(self, didGetDetails: [], withSuccess: false)
                return
            }
            if let model = try? JSONDecoder().decode(Details.self, from: response ?? Data()) {
                self.delegate?.detailsDataProvider(self, didGetDetails: model.items, withSuccess: true)
            }
        }
    }
}
protocol DetailsDataProviderDelegate {
    func detailsDataProvider(_ dataProvider: DetailsDataProvider, didGetDetails itens: [Items], withSuccess success: Bool)
}

extension DetailsDataProviderDelegate {
    func detailsDataProvider(_ dataProvider: DetailsDataProvider, didGetDetails itens: [Items], withSuccess success: Bool) {}
}
