//
//  VideosDataProvider.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import Foundation

class VideosDataProvider {
    
    var delegate: VideosDataProviderDelegate?
    
    func getVideos(bySearchString string: String? = nil) {
        let apiManager = ApiManager()
        var parameters = ["part" : "id,snippet"]
        
        if string != nil {
            parameters["q"] = string!
        }
        
        apiManager.get(Endpoint.getVideos, parameters: parameters) { (success, response) in
            if !success {
                self.delegate?.videosDataProvider(self, didGetVideos: [], withSuccess: false)
                return
            }
            
            if let model = try? JSONDecoder().decode(VideosResponse.self, from: response ?? Data()) {
                self.delegate?.videosDataProvider(self, didGetVideos: model.items, withSuccess: true)
            } else {
                self.delegate?.videosDataProvider(self, didGetVideos: [], withSuccess: false)
            }
        }
    }
    
    
    
}

protocol VideosDataProviderDelegate {
    func videosDataProvider(_ dataProvider: VideosDataProvider, didGetVideos videos: [Video], withSuccess success: Bool)
}

extension VideosDataProviderDelegate {
    func videosDataProvider(_ dataProvider: VideosDataProvider, didGetVideos videos: [Video], withSuccess success: Bool) {}
}
