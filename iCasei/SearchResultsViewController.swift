//
//  SearchResultsViewController.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import UIKit

class SearchResultsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var results: [Video] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 476
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0,
                                              left: 0,
                                              bottom: 58 + (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0),
                                              right: 0)
    }
    
}

extension SearchResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell", for: indexPath) as? ResultTableViewCell {
            if let parent = parent as? ViewController {
                cell.delegate = parent
            }
            cell.fillWith(results[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let aViewDeDetalhesQueQueroNavegar = DetailsResultsViewController()
        parent?.navigationController?.pushViewController(aViewDeDetalhesQueQueroNavegar, animated: true)
    }
}
