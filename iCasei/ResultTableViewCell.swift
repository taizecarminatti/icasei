//
//  ResultTableViewCell.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import UIKit
import SDWebImage

class ResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var videoNameLabel: UILabel!
    @IBOutlet weak var channelNameLabel: UILabel!
    @IBOutlet weak var videoDescriptionLabel: UILabel!
    @IBOutlet weak var detailsButton: UIButton!
    
    var video: Video?
    
    var delegate: ResultTableViewCellDelegate?
    
    func fillWith(_ video: Video) {
        self.video = video
        thumbImageView.sd_setImage(with: URL(string: video.snippet.thumbnails.medium.url), placeholderImage: UIImage())
        videoNameLabel.text = video.snippet.title
        channelNameLabel.text = video.snippet.channelTitle
        videoDescriptionLabel.text = video.snippet.description
    }
    
    @IBAction func detailsButton_Action(_ sender: UIButton) {
        print("Detalhes do vídeo: \(video?.id.videoId ?? "")")
        delegate?.resultTableViewCell(self, didPressDetailsButton: sender)
    }
    
}

protocol ResultTableViewCellDelegate {
    func resultTableViewCell(_ cell: ResultTableViewCell, didPressDetailsButton button: UIButton)
}
