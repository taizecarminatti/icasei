//
//  Video.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import Foundation
import Alamofire

struct Video: Codable {
    let kind, etag: String
    let id: ID
    let snippet: Snippet
}

struct ID: Codable {
    let kind, videoId: String
}

struct Snippet: Codable {
    let publishedAt, channelId, title, description: String
    let thumbnails: Thumbnails
    let channelTitle, liveBroadcastContent: String
}

struct Thumbnails: Codable {
    let medium, high: Default
}

struct Default: Codable {
    let url: String
    let width, height: Int
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseVideo(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Video>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
