//
//  DetailsViewController.swift
//  iCasei
//
//  Created by Taize Carminatti on 11/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import Foundation
import Alamofire

struct Details: Codable {
    let kind, etag: String
    let pageInfo: PageInfo
    let items: [Items]
    
}
struct Items : Codable {
    let kind, etag, id: String
    let snippet: SnippetDetails
    let statistics: Statistics?
}

struct Statistics: Codable {
    let viewCount, likeCount, dislikeCount, favoriteCount, commentCount: String
}

struct SnippetDetails: Codable {
    let publishedAt, channelId, title, description: String
    let thumbnails: Thumbnails
    let channelTitle, liveBroadcastContent: String
    let localized: Localized
}

struct Localized: Codable {
    let title, description: String
}


fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

