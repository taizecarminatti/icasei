//
//  ApiManager.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    
    func addCustomHeaders(_ headers: [String: Any]) -> HTTPHeaders {
        if var newHeaders = headers as? HTTPHeaders {
            newHeaders["X-Origin"] = "https://explorer.apis.google.com"
            return newHeaders
        } else {
            return [:]
        }
    }
    
    func addAuth(_ parameters: [String: Any]) -> [String: Any] {
        var newParameters = parameters
        newParameters["key"] = Defaults.youtubeApiKey
        return newParameters
    }
    
    func get(_ endpoint: Endpoint, parameters: [String: Any], completion: @escaping (_ success: Bool, _ responseBody: Data?) -> ()) {
        guard let url = URL(string: endpoint.rawValue) else { return }
        let urlRequest = URLRequest(url: url)
        guard let encodedURLRequest = try? URLEncoding.queryString.encode(urlRequest, with: addAuth(parameters)) else { return }
        
        let req = Alamofire.request(
            encodedURLRequest.url!,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: addCustomHeaders([:]))
        
        debugPrint(req)
        
        req.responseJSON { (response) in
//            debugPrint(response)
            
            if response.result.isSuccess {
                if let dict = response.result.value as? [String: Any?] {
                    if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
                        print("*********** RESPONSE ***********")
                       print(String(data: jsonData, encoding: .utf8))
                        completion(true, jsonData)
                        return
                    }
                    completion(false, nil)
                    return
                }
            }

        }
        
    }
    
}
