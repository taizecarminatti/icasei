//
//  Endpoint.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import Foundation

enum Endpoint: String {
    case getVideos = "https://content.googleapis.com/youtube/v3/search"//?part=id%2Csnippet&key={youtubeApiKey}&q={search}"
    case getDetails = "https://content.googleapis.com/youtube/v3/videos"
}


