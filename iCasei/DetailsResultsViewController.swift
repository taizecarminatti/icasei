//
//  DetailsResultsViewController.swift
//  iCasei
//
//  Created by Taize Carminatti on 12/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import UIKit

class DetailsResultsViewController: BaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbLike: UILabel!
    @IBOutlet weak var lbDislike: UILabel!
    @IBOutlet weak var lbVision: UILabel!
    @IBOutlet weak var tvDescriptionTextView: UITextView!
    @IBOutlet weak var lbNameChanel: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    
    var item: Items?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDetails()
    }
    
    func setDetails() {
        guard let url = URL(string: item?.snippet.thumbnails.medium.url ?? "") else { return }
        let data = try? Data(contentsOf: url)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            imageView.image = image
        }
        
        tvDescriptionTextView.text = item?.snippet.description
        lbDislike.text = item?.statistics?.dislikeCount
        lbLike.text = item?.statistics?.likeCount
        lbVision.text = item?.statistics?.viewCount
        lbNameChanel.text = item?.snippet.channelTitle
        lbTitle.text = item?.snippet.title
        navigationController?.navigationBar.backItem?.title = item?.snippet.title
    }
  
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
}
