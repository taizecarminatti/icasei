//
//  ViewController.swift
//  iCasei
//
//  Created by Taize Carminatti on 10/05/19.
//  Copyright © 2019 iCasei. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchView: UIView!
    
    let videosDataProvider = VideosDataProvider()
    let detailsDataProvider = DetailsDataProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
        videosDataProvider.delegate = self
        detailsDataProvider.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchView.layer.cornerRadius = 25
        searchView.layer.borderColor = UIColor.black.cgColor
        searchView.layer.borderWidth = 1
    }
    
    @IBAction func searchButton_Action() {
        view.endEditing(true)
        videosDataProvider.getVideos(bySearchString: searchTextField.text)
    }
    
    func viewError() {
        UIView.animate(withDuration: 0.25, delay: 0.1, options: .curveEaseIn, animations: {
            self.searchView.frame = CGRect(x: 10,
                                           y: 0 + (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0) + 44,
                                           width: UIScreen.main.bounds.width - 20,
                                           height: self.searchView.frame.size.height)
            self.searchView.layer.cornerRadius = 0
            self.view.layoutIfNeeded()
        }) { (finished) in
            if let errorViewController = self.storyboard?.instantiateViewController(withIdentifier: "ErrorViewController") as? ErrorViewController {
                errorViewController.view.frame = CGRect(x: 0,
                                                                y: (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0) + 58 + 44,
                                                                width: UIScreen.main.bounds.width,
                                                                height: UIScreen.main.bounds.height - 58 - 44)
            
                self.addChild(errorViewController)
                self.view.addSubview(errorViewController.view)
            }
        }
    }
}


extension ViewController: VideosDataProviderDelegate {
    func videosDataProvider(_ dataProvider: VideosDataProvider, didGetVideos videos: [Video], withSuccess success: Bool) {
        searchView.translatesAutoresizingMaskIntoConstraints = true
        
        if !videos.isEmpty {
            UIView.animate(withDuration: 0.25, delay: 0.1, options: .curveEaseIn, animations: {
                self.searchView.frame = CGRect(x: 10,
                                               y: 0 + (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0) + 44,
                                               width: UIScreen.main.bounds.width - 20,
                                               height: self.searchView.frame.size.height)
                self.searchView.layer.cornerRadius = 0
                self.view.layoutIfNeeded()
            }) { (finished) in
                if let searchResultsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsViewController") as? SearchResultsViewController {
                    searchResultsViewController.view.frame = CGRect(x: 0,
                                                                    y: (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0) + 58 + 44,
                                                                    width: UIScreen.main.bounds.width,
                                                                    height: UIScreen.main.bounds.height - 58 - 44)
                    searchResultsViewController.results = videos
                    self.addChild(searchResultsViewController)
                    self.view.addSubview(searchResultsViewController.view)
                }
            }
        } else {
            viewError()
        }
    }
}

extension ViewController: DetailsDataProviderDelegate {
    func detailsDataProvider(_ dataProvider: DetailsDataProvider, didGetDetails items: [Items], withSuccess success: Bool) {
        
        if let detailsResultsViewController = self.storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as? DetailsResultsViewController {
            
            if let item = items.first {
                detailsResultsViewController.item = item
            }
            navigationController?.pushViewController(detailsResultsViewController, animated: true)
//            self.addChild(detailsResultsViewController)
//            self.view.addSubview(detailsResultsViewController.view)
        }
    }
}


extension ViewController: ResultTableViewCellDelegate {
    func resultTableViewCell(_ cell: ResultTableViewCell, didPressDetailsButton button: UIButton) {
        detailsDataProvider.getDetails(bySearchString: cell.video?.id.videoId) 
    }
}
